@extends(env('THEME').'.layouts.site')

@section('content') 
     
	 <!-- Information Store -->
		@section('information_store')
			@include(env('THEME').'.store.section_information_store') 
		@show
	<!-- END Information Store -->

   @foreach($sectionsContentPage as $key => $sectionContent)
		{!! $sectionContent !!}
   @endforeach
@endsection

		