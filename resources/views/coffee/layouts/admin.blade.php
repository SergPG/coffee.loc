<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="{{ asset(env('THEME'))}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{ asset(env('THEME'))}}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{ asset(env('THEME'))}}/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  
  <script type="text/javascript" src="{{ asset('js/ckeditor/ckeditor.js')}}"></script>
  
  <!-- Custom styles for this template-->
  <link href="{{ asset(env('THEME'))}}/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    
	<!-- Admin Navigation -->
		@section('admin_navigation')
			@include(env('THEME').'.layouts.design.admin.admin_navigation') 
		@show
	<!-- END Admin Navigation -->
	
	<div class="content-wrapper">	
  		<div class="container-fluid">
		   
		@section('admin_breadcrumb')
			@include(env('THEME').'.layouts.design.admin.admin_breadcrumb') 
		@show
				
	<!-- Content -->	
		@yield('content')
	<!-- END Content -->	
			
	  	</div>
		<!-- /.container-fluid-->
    <!-- /.content-wrapper-->
	
	<!-- Admin Footer -->
		@section('admin_footer')
			@include(env('THEME').'.layouts.design.admin.admin_footer') 
		@show
	<!-- END Admin Footer -->
	
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
	
	<!-- Logout Modal-->
		@section('admin_logout_modal')
			@include(env('THEME').'.layouts.design.admin.admin_logout_modal') 
		@show
	
  </div>	
   <!-- Bootstrap core JavaScript-->
    <script src="{{ asset(env('THEME'))}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset(env('THEME'))}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset(env('THEME'))}}/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{ asset(env('THEME'))}}/vendor/chart.js/Chart.min.js"></script>
    <script src="{{ asset(env('THEME'))}}/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{ asset(env('THEME'))}}/vendor/datatables/dataTables.bootstrap4.js"></script>
	
	
	
    <script src="{{ asset(env('THEME'))}}/vendor/bootstrap-filestyle.min.js"></script>
	
    
    <!-- Custom scripts for all pages-->
    <script src="{{ asset(env('THEME'))}}/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="{{ asset(env('THEME'))}}/js/sb-admin-datatables.min.js"></script>
    <script src="{{ asset(env('THEME'))}}/js/sb-admin-charts.js"></script>

</body>

</html>