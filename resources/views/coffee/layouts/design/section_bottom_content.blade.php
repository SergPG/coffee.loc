
	<section class="page-section about-heading">
      <div class="container">
	  
	    @if(!empty($article->image))
        <img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" style = "max-height:45vw;width:100%;" src="{{ asset(env('THEME'))}}/img/pages/{{$article->image}}" alt="">
        @endif
		
		<div class="about-heading-content">
          <div class="row">
            <div class="col-xl-9 col-lg-10 mx-auto">
              <div class="bg-faded rounded p-5">
                <h2 class="section-heading mb-4">
                  <span class="section-heading-upper">{{$article->heading_upper}}</span>
				  <span class="section-heading-lower">{{$article->heading_lower}}</span>
                </h2>
						{!!$article->text!!}
					
              </div>	
            </div>
          </div>
        </div>
		
		@if(!empty($article->link_url))
		<div class="intro intro-button text-center mx-auto" style="margin-top: -2rem; z-index:10;"  >
			<a class="btn btn-primary btn-xl" href="{{$article->link_url}}" >{{$article->link_text}}</a>
		</div>
		@endif	
		
      </div>
    </section>