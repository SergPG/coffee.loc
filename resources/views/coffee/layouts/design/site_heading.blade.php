 
	<h1 class="site-heading text-center text-white d-none d-lg-block">
      <span class="site-heading-upper text-primary mb-3">{{config('setting.site_slogan')}}</span>
      <span class="site-heading-lower">{{config('setting.site_name')}}</span>
    </h1>