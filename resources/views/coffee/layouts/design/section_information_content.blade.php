
	<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            
			<div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-4">
                <span class="section-heading-upper">{{$article->heading_upper}}</span>
				<span class="section-heading-lower">{{$article->heading_lower}}</span>
              </h2>
					{!!$article->text!!}
				
				@if(!empty($article->link_url))
				<div class="intro intro-button mt-3 mx-auto"  >
					<a class="btn btn-primary btn-xl" href="{{$article->link_url}}" >{{$article->link_text}}</a>
				</div>
				@endif
				
            </div>
			
          </div>
        </div>
      </div>
    </section>