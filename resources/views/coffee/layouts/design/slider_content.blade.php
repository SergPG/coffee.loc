<?
// $slider
// slider_content   section_left_content  section_bottom_content
?>
<!-- Section Slides Top -->
    
    
    @if(isset($sliders) && is_object($sliders)&& (count($sliders)>0))
		<section class="flexslider">
			<ul class="slides">
				@foreach($sliders as $k => $slider)
					<div class="container">
					<!-- ------------------ -->
							<div class="intro">
						  @if(!empty($slider->image))
						  <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="{{ asset(env('THEME'))}}/img/pages/{{$slider->image}}" alt="">
						  @endif
						  <div class="intro-text left-0 text-center bg-faded p-5 rounded">
							<h2 class="section-heading mb-4">
							  <span class="section-heading-upper">{{$slider->heading_upper}}</span>
							  <span class="section-heading-lower">{{$slider->heading_lower}}</span>
							</h2>
							<p class="mb-3"> {{ $slider->description }} </p>
							
							@if(!empty($slider->link_url))
							<div class="intro-button mx-auto">
							  <a class="btn btn-primary btn-xl" href="{{$slider->link_url}}">{{$slider->link_text}}</a>
							</div>
							@endif
						  </div>
						</div>
					<!-- ------------------ -->	
						</div>
					</li>
				@endforeach     
			</ul>
		</section>
	@endif
	 <!-- END Section Slides Top -->





	<section class="page-section clearfix">
      <div class="container">
        
      </div>
    </section>