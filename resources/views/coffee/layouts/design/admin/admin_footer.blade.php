    
	<footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright &copy; {{config('setting.site_name')}} 2018</small>
        </div>
      </div>
    </footer>