    
	<!-- Upload Image Modal-->
    <div class="modal fade" id="uploadImageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

{!! Form::open(['url' => url('admin/uploadImage'),'id'=>'form_upload_imege', 'enctype' => 'multipart/form-data' ]) !!}
		
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
		  
			<div class="form-group"> 
			{!! Form::file('img',['id'=>'img', 'name'=>'img' ,'class' => 'form-control filestyle'])  !!}
			</div>
		  
		  </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            
			{!! Form::submit('Click Me!')!!} 	
			
          </div>
{!! Form::close() !!}			  
		  
        </div>
      </div>
    </div>