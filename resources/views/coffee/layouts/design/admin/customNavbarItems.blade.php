    
  @foreach($items as $item)
        
		
		
		<li class="nav-item {{($item->active)? 'active':''}}" data-toggle="tooltip" data-placement="right" title="{{$item->title}}">
         @if($item->hasChildren()) 
		  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" data-parent="#exampleAccordion" href="#collapse{{$item->nickname()}}">
            <i class="fa fa-fw {{ $item->nickname() }}"></i>
            <span class="nav-link-text">{{$item->title}}</span>
          </a>
		  
		  <ul class="sidenav-second-level collapse" id="collapse{{$item->nickname()}}">
		       @foreach($item->children() as $chItem)
			       <li>
						<a  href="{{$chItem->url()}}">{{$chItem->title}}</a>
					</li>
			   @endforeach
		  </ul>
		  
		  @else
          
		  <a class="nav-link" href="{{$item->url()}}">
            <i class="fa fa-fw {{ $item->nickname() }}"></i>
            <span class="nav-link-text">{{$item->title}}</span>
          </a>
		  
		</li>	
        @endif
  
  
  @endforeach
	
	
	
	
	