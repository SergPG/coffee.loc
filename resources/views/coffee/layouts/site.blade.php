<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{$metaTitle}}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset(env('THEME'))}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset(env('THEME'))}}/css/business-casual.min.css" rel="stylesheet">

  </head>

  <body>
    
	<!-- Site Beading -->
		@section('site_heading')
			@include(env('THEME').'.layouts.design.site_heading') 
		@show
	<!-- END Site Beading -->
 
	<!-- Navigation -->
	    @section('site_navigation')
			@include(env('THEME').'.layouts.design.site_navigation') 
		@show 
	<!-- END Navigation -->

	
	<!-- Content -->	
		@yield('content')
	<!-- END Content -->
	
	
    
	<!-- Footer -->
		@section('footer')
		<footer class="footer text-faded text-center py-5">
			@include(env('THEME').'.layouts.design.footer') 
		</footer>
		@show
	<!-- END Footer -->
	
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset(env('THEME'))}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset(env('THEME'))}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>
  
  <!-- Script to highlight the active date in the hours list -->
  <script>
    $('.list-hours li').eq(new Date().getDay()).addClass('today');
  </script>

</html>
