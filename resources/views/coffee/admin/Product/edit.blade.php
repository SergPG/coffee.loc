
	@extends(env('THEME').'.layouts.admin')
 
<!-- Section Content-->	

	@section('content') 
	
<section class="page-section">
 <div class="card mb-3">
   <div class="card-header">
	<i class="fa fa-lg fa-calendar  mr-2"></i> {{$product->created_at->format(config('coffee.date_format')) }}
   </div> <!-- .card-header  -->
	
 
 {!! Form::open(['url' => url('admin/products',['id'=>$product->id]),   'id'=>'form_edit', 'enctype' => 'multipart/form-data' ]) !!}
    {{ method_field('PUT') }}
   <div class="card-body">
   
 <div class="form-group">
   {!! Form::label('heading_upper', 'Заголовок')  !!} 
   {!! Form::text('heading_upper',$product->heading_upper,['class' => 'form-control'])  !!}
 </div>
 <div class="form-group">
   {!! Form::label('heading_lower', 'Подзаголовок')  !!} 
   {!! Form::text('heading_lower',$product->heading_lower,['class' => 'form-control'])  !!}
 </div>
 
 @if(!empty($product->image))
 <div class="form-group">
   {!! Form::label('image', 'Рисунок')  !!} 
   {!! Form::hidden('image',(session('image_name'))? session('image_name'):$image_name,['class' => 'form-control'])  !!}
   
 </div>
 
 <div class="form-group"> 
		<a class="btn btn-success mr-2" data-toggle="modal" data-target="#uploadImageModal" role="button" title="Upload File" href="#uploadImageModal" >
            <i class="fa fa-lg fa-edit mr-2"></i>Изменить</a> 
	
 </div>
	@if (session('status'))
  <div class="alert alert-success">
    {{ session('status') }}
  </div>
	@endif
       
   @if (session('image_name'))
  <div class="alert alert-success">
    {{ session('image_name') }}
  </div>
	@endif
 <div class="form-group">
 
     
	<img class="product-item-img mx-auto d-flex rounded img-fluid mb-3 mb-lg-0" src="{{ asset(env('THEME'))}}/img/products/{{(session('image_name'))? session('image_name'):$image_name}}" alt="">
 </div>
 @endif
 
 <div class="form-group">
   {!! Form::label('description1', 'Описание')  !!} 
 
   {!! Form::textarea('description',$product->description,['id'=>'description','class' => 'form-control', ])  !!}

   
 </div>
 
   </div> <!-- .card-body  -->
   
   <div class="card-footer small text-right">
 {!! Form::submit('Click Me!')!!} 	
   
		  <a class="btn btn-success mr-2" data-toggle="tooltip" role="button" title="Edit" href="{{ url('admin/products',['id'=>$product->id, 'edit'] ) }}"><i class=" fa fa-lg fa-edit"></i></a>
									
		   <a class="btn btn-danger mr-2" data-toggle="tooltip" role="button" title="Delete" href="#"><i class="fa fa-lg  fa-trash"></i></a>
   
   
       
		
		
		</div>
{!! Form::close() !!}		
  </div> <!-- .card  -->
</section>
     
	@include(env('THEME').'.layouts.design.admin.admin_Upload_Image_Modal')  
	 
	 
<script>
   CKEDITOR.replace('description');
 </script>
    	
		
		
		
	@endsection

    
