
	@extends(env('THEME').'.layouts.admin')
 
<!-- Section Content-->	

	@section('content') 
	
<section class="page-section">
 <div class="card mb-3">
   <div class="card-header">
	<i class="fa fa-lg fa-calendar  mr-2"></i> 20.03.2018
   </div> <!-- .card-header  -->
	
 
 {!! Form::open(['url' => url('admin/products'),'method'=>'POST','id'=>'form_create', 'enctype' => 'multipart/form-data' ]) !!}
    
   <div class="card-body">
   
   
   
 <div class="form-group">
   {!! Form::label('heading_upper', 'Заголовок')  !!} 
   {!! Form::text('heading_upper',old('heading_upper'),['class' => 'form-control'])  !!}
   

@if ($errors->has('heading_upper'))
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->get('heading_upper') as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



 </div>
 <div class="form-group">
   {!! Form::label('heading_lower', 'Подзаголовок')  !!} 
   {!! Form::text('heading_lower',old('heading_lower'),['class' => 'form-control'])  !!}
 </div>
 
 
 <div class="form-group">
   {!! Form::label('image', 'Рисунок')  !!} 
   {!! Form::text('image',(session('image_name'))? session('image_name'):old('image'),['class' => 'form-control'])  !!}
   
 </div>
 
 <div class="form-group"> 
		<a class="btn btn-success mr-2" data-toggle="modal" data-target="#uploadImageModal" role="button" title="Upload File" href="#uploadImageModal" >
            <i class="fa fa-lg fa-edit mr-2"></i>Изменить</a> 
	
 </div>
	@if (session('status'))
  <div class="alert alert-success">
    {{ session('status') }}
  </div>
	@endif
	
 @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	
 <div class="form-group">
 
    
 </div>

 
 <div class="form-group">
   {!! Form::label('description', 'Описание')  !!} 
 
   {!! Form::textarea('description',old('description'),['id'=>'description','class' => 'form-control', ])  !!}

   
 </div>
 
   </div> <!-- .card-body  -->
   
   <div class="card-footer small text-right">
 {!! Form::submit('Click Me!')!!} 	
   
		  
   
		
		
		</div>
{!! Form::close() !!}		
  </div> <!-- .card  -->
</section>
     
	@include(env('THEME').'.layouts.design.admin.admin_Upload_Image_Modal')  
	 
	 
<script>
   CKEDITOR.replace('description');
 </script>
    	
		
		
		
	@endsection

    
