    
 <!-- Card Columns Example Social Feed-->
	<div class="mb-0 mt-4">
	 <i class="fa fa-newspaper-o"></i> {{ $title }} Feed
	</div>
	 <hr class="mt-2">
	 
	
	
	   @foreach($articles as $key => $article)
	    <!-- Example Social Card-->
	    <div class="card mb-3">
		
		<div class="card-body">
			<section class="page-section clearfix">
      <div class="container">
        <div class="intro">
		  @if(!empty($article->image))
          <img class="intro-img img-fluid mb-3 mb-lg-0 rounded"  src="{{ asset(env('THEME'))}}/img/pages/{{$article->image}}" alt="">
	      @endif
          <div class="intro-text left-0 text-center bg-faded p-5 rounded">
            <h2 class="section-heading mb-4">
              <span class="section-heading-upper">{{$article->heading_upper}}</span>
              <span class="section-heading-lower">{{$article->heading_lower}}</span>
            </h2>
				{!!$article->text!!}
            
			@if(!empty($article->link_url))
            <div class="intro-button mx-auto">
              <a class="btn btn-primary btn-xl" href="{{$article->link_url}}">{{$article->link_text}}</a>
            </div>
			@endif
          </div>
        </div>
      </div>
    </section>
		</div>
		  <hr class="my-0">
		
	@endforeach	 
		  <div class="card-footer small text-muted">Posted 32 mins ago</div>
	  </div>
					
					
					

	
	