   
   <!-- Example DataTables Card-->
	<div class="card mb-3">
				<div class="card-header">
				  <i class="fa fa-table"></i> {{ $title }} Table </div>
				<div class="card-body">
				  <div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					  <thead>
						<tr>
						  <th width="15%">Article Title</th>
						  <th width="15%">Article Slogan</th>
						  <th>Description</th>
						  <th>Image</th>
						  <th>Create date</th>
						  <th></th>
						  <th></th>
						</tr>
					  </thead>
					  <tfoot>
						<tr>
						   <th>Article Title</th>
						  <th>Article Slogan</th>
						  <th>Description</th>
						  <th>Image</th>
						  <th>Create date</th>
						  <th></th>
						  <th></th>
						</tr>
					  </tfoot>
					  <tbody>
	    
    @if(isset($articles) && is_object($articles))
		
	   @foreach($articles as $key => $article)
	        <tr>
			  <td> {{ $article->heading_upper }} </td>
			  <td> {{ $article->heading_lower }} </td>
			  <td> {!! $article->description !!}</td>
			  <td>{{ $article->image }}</td>
			  <td>2011/04/25</td>
			  <td><a class="btn btn-success" data-toggle="tooltip"  title="Edit" href="#"><i class="fa fa-fw fa-edit"></i></a></td>
			  <td><a class="btn btn-danger" data-toggle="tooltip"  title="Delete" href="#"><i class="fa fa-fw fa-trash"></i></a></td>
			</tr>
	   
	   @endforeach
	
	
						
    @endif		
					  
					  
						
					  </tbody>
					</table>
				  </div>
				</div>
				<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
	</div>
	<!-- Example DataTables Card-->
			  