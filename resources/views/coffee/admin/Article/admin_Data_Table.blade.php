   
   <!-- Example DataTables Card-->
 
	<div class="card mb-3">
				<div class="card-header">
				  <i class="fa fa-table"></i> {{ $title }} Table </div>
				<div class="card-body">
				  <div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					  <thead>
						<tr>
						  <th>Articles</th>
						</tr>
					  </thead>
					  <tfoot>
						<tr>
						  <th>Articles</th>
						</tr>
					  </tfoot>
					  <tbody>
	    
    @if(isset($articles) && is_object($articles))
		
	   @foreach($articles as $key => $article)
	        <tr>
			  <td> 
			   
			    <div class="card mb-3 card-columns">
				 <div class="card-header">
					  <i class="fa fa-lg fa-calendar  mr-2"></i> {{$article->created_at->format(config('coffee.date_format')) }}
				  </div>
				   @if(!empty($article->image))
					<a href="#">
						<img class="card-img-top img-fluid w-100 " src="{{ asset(env('THEME'))}}/img/pages/{{$article->image}}" alt="">
					</a>
				   @endif	
				 <div class="card-body">
					<a href="#">
					  <h6 class="card-title mb-1">{{$article->heading_upper}}</h6>
					  <h6 class="card-title mb-1">{{$article->heading_lower}}</h6>
					</a>
					<p class="card-text small">
						{!!$article->description !!}
					</p>
				 </div> 
				 <div class="card-footer small text-right">
					
					   <a class="btn btn-success mr-2" data-toggle="tooltip" role="button" title="Edit" href="#"><i class=" fa fa-lg fa-edit"></i></a>
									
					<a class="btn btn-danger mr-2" data-toggle="tooltip" role="button" title="Delete" href="#"><i class="fa fa-lg  fa-trash"></i></a>
					 
				 
				 </div>
				</div>
				
			  
			  </td>
			  
			</tr>
	   
	   @endforeach
	
	
						
    @endif		
					  
					  
						
					  </tbody>
					</table>
				  </div>
				</div>
				<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
	</div>
	
	<!-- Example DataTables Card-->
			  