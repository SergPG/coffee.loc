
	@extends(env('THEME').'.layouts.admin')

<!-- Section Content-->	

	@section('content')   
        
        @if(isset($icon_cards))
			@section('admin_icon_cards')
				@include(env('THEME').'.layouts.design.admin.admin_icon_cards') 
			@show          
        @endif			
		
		
		
		
		@if(isset($area_chart))	 	
			@section('admin_Area_Chart')
				@include(env('THEME').'.layouts.design.admin.admin_Area_Chart') 
			@show	
       	 @endif	 
			  
	<div class="row">
		<div class="col-lg-8">
		
		@if(isset($bar_chart))	
			@section('admin_Bar_Chart')
				@include(env('THEME').'.layouts.design.admin.admin_Bar_Chart') 
			@show
		@endif	
			  
		
        @if(isset($news_feed))	
			<!-- Card Columns Example Social Feed-->
			@section('admin_News_Feed')
				@include(env('THEME').'.layouts.design.admin.admin_News_Feed') 
			@show		
		@endif			
				    
				  
		</div> <!-- .col-lg-8  -->
		
		<div class="col-lg-4">
		
		@if(isset($notifications_feed))		
			@section('admin_Notifications_Feed')
				@include(env('THEME').'.layouts.design.admin.admin_Notifications_Feed') 
			@show
		@endif
		
		@if(isset($pie_chart))		
			@section('admin_Pie_Chart')
				@include(env('THEME').'.layouts.design.admin.admin_Pie_Chart') 
			@show	
		@endif
		
				 
				 
		</div> <!-- .col-lg-4  -->
	</div>  <!-- .row -->
	    
		@if(isset($data_table))
			@section('admin_Data_Table')
				@include(env('THEME').'.layouts.design.admin.admin_Data_Table') 
			@show       
        @endif			
			  
		
	
@endsection

    
