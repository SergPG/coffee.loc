
	<section class="page-section">
      <div class="container">
        <div class="product-item">
		
          <div class="product-item-title d-flex">
            <div class="bg-faded p-5 d-flex {{ ($key%2 == 0)? 'ml':'mr' }}-auto rounded">
              <h2 class="section-heading mb-0">
                <span class="section-heading-upper">{{$product->heading_upper}}</span>
				<span class="section-heading-lower">{{$product->heading_lower}}</span>
              </h2>
            </div>
          </div>
          
		  @if(!empty($product->image))
		  <img class="product-item-img mx-auto d-flex rounded img-fluid mb-3 mb-lg-0" src="{{ asset(env('THEME'))}}/img/products/{{$product->image}}" alt="">
	      @endif
		  
          <div class="product-item-description d-flex {{ ($key%2 == 0)? 'mr':'ml' }}-auto">
            <div class="bg-faded p-5 mb-3 rounded">
							{!!$product->description!!}
            </div>	
          </div>
		  
		  @if(!empty($product->link_url))
		  <div class="product-item-description text-center intro-button {{ ($key%2 == 0)? 'mr':'ml' }}-auto" style="z-index:10;"  >
			<a class="btn btn-primary btn-xl" href="{{$product->link_url}}" >{{$product->link_text}}</a>
		  </div>
		  @endif	
			
        </div>
      </div>
    </section>

    