<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', env('THEME').'\Home\HomeController@index')->name('home');

Route::get('/about', env('THEME').'\About\AboutController@index')->name('about');

Route::get('/products', env('THEME').'\Product\ProductController@index')->name('products');

Route::get('/store', env('THEME').'\Store\StoreController@index')->name('store');

Auth::routes();

// Admin
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	//admin
	Route::get('/',env('THEME').'\Admin\DashboardController@index')->name('admin');
	
	Route::get('/uploadImage',env('THEME').'\Admin\UploadImageController@formUploadImage')->name('form_upload_image');
	
	Route::post('/uploadImage',env('THEME').'\Admin\UploadImageController@storeUploadImage')->name('store_upload_image');
	
	
	
 Route::group(['prefix'=>'components'],function(){
	//components/cards
	Route::get('cards',env('THEME').'\Admin\CardController@index')->name('components.cards');
 });
  
	//articles
	Route::resource('articles',env('THEME').'\Admin\ArticleController');
	
	//products
	Route::resource('products',env('THEME').'\Admin\ProductController');
	
	//users
	Route::resource('users',env('THEME').'\Admin\UserController');
	
	//pages
	Route::resource('pages',env('THEME').'\Admin\PageController');
	
 
	
});




Route::get('/home', 'HomeController@index')->name('home1');


