<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Site Setting
    |--------------------------------------------------------------------------
    |
    |
    */

    'site_name' =>  'Coffee House',
	
    'site_slogan' =>  'Best coffee &amp; tea',
	
	
	'date_format' => 'd m Y',
	
	'design_product' => 'section_lists_content',
	
	//D:\ospanel\domains\coffee.loc\public\coffee\img\products
	
	'path_img_product' => public_path('coffee\img\products'),

];
