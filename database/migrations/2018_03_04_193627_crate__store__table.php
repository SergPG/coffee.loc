<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store', function (Blueprint $table) {
            $table->increments('id');
			
			$table->string('name_options')->unique();
			$table->json('value_options');
			 /*
	   store_time 
	 {"open_store":["false","07:00","07:00","07:00","07:00","07:00","09:00"],
	 "close_store":["false","18:00","18:00","18:00","18:00","18:00","17:00"]}
		[
			{'day': "Monday", "open": "07:00", "close": "18.00"},
			{'day': "Sunday", "open": "closed", "close": "closed"}
		]
		{'value': "Test"}
 	   
				
			 */
			 /*
			$table->json('address');
					
				 {"streetAddress": "1116 Orchard Street","city": "Golden Valley", "region": "Minnesota","phoneNumbers":["(317) 585-8468","(916) 123-4567"]}
			 
			 
			 */
				
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store');
    }
}
