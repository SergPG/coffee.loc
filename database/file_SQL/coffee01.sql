-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 01 2018 г., 00:31
-- Версия сервера: 5.7.20
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `coffee`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tabarticles`
--

CREATE TABLE `tabarticles` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading_upper` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading_lower` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabarticles`
--

INSERT INTO `tabarticles` (`id`, `heading_upper`, `heading_lower`, `description`, `text`, `image`, `link_text`, `link_url`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Fresh Coffee', 'Worth Drinking', '<p class=\"mb-3\">Every cup of our quality artisan coffee starts with locally sourced, hand picked ingredients. Once you try it, our coffee will be a blissful addition to your everyday morning routine - we guarantee it! </p>', '<p class=\"mb-3\">Every cup of our quality artisan coffee starts with locally sourced, hand picked ingredients. Once you try it, our coffee will be a blissful addition to your everyday morning routine - we guarantee it! </p>', 'intro.jpg', 'Visit Us Today!', '/', 'article-1', '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(2, 'Our Promise', 'To You', '<p class=\"mb-0\">When you walk into our shop to start your day, we are dedicated to providing you with friendly service, a welcoming atmosphere, and above all else, excellent products made with the highest quality ingredients. If you are not satisfied, please let us know and we will do whatever we can to make things right!</p>', '<p class=\"mb-0\">When you walk into our shop to start your day, we are dedicated to providing you with friendly service, a welcoming atmosphere, and above all else, excellent products made with the highest quality ingredients. If you are not satisfied, please let us know and we will do whatever we can to make things right!</p>', NULL, NULL, NULL, 'article-2', '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(3, 'Strong Coffee, Strong Roots', 'About Our Cafe', '<p>Founded in 1987 by the Hernandez brothers, our establishment has been serving up rich coffee sourced from artisan farmers in various regions of South and Central America. We are dedicated to travelling the world, finding the best coffee, and bringing back to you here in our cafe.</p>', ' <p>Founded in 1987 by the Hernandez brothers, our establishment has been serving up rich coffee sourced from artisan farmers in various regions of South and Central America. We are dedicated to travelling the world, finding the best coffee, and bringing back to you here in our cafe.</p>\r\n  <p class=\"mb-0\">We guarantee that you will fall in <em>lust</em> with our decadent blends the moment you walk inside until you finish your last sip. Join us for your daily routine, an outing with friends, or simply just to enjoy some alone time.</p>', 'about.jpg', NULL, NULL, 'article-abuot-1', '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(4, 'Blended to Perfection', 'Coffees &amp; Teas', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', 'products-01.jpg', NULL, NULL, 'article-product-1', '2018-02-27 22:00:00', '2018-02-27 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabarticle_page`
--

CREATE TABLE `tabarticle_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabarticle_page`
--

INSERT INTO `tabarticle_page` (`id`, `article_id`, `page_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(2, 2, 1, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(3, 3, 2, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(4, 4, 3, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(5, 3, 4, '2018-02-27 22:00:00', '2018-02-27 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabmigrations`
--

CREATE TABLE `tabmigrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabmigrations`
--

INSERT INTO `tabmigrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_28_125905_create__pages__table', 1),
(4, '2018_02_28_130954_add__parent__pages__table', 2),
(5, '2018_02_28_132855_create__articles__table', 3),
(6, '2018_02_28_143341_create__article__page__table', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `tabpages`
--

CREATE TABLE `tabpages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabpages`
--

INSERT INTO `tabpages` (`id`, `name`, `alias`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(2, 'About', 'about', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(3, 'Products', 'products', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(4, 'Store', 'store', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabpassword_resets`
--

CREATE TABLE `tabpassword_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tabusers`
--

CREATE TABLE `tabusers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tabarticles`
--
ALTER TABLE `tabarticles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_alias_unique` (`alias`);

--
-- Индексы таблицы `tabarticle_page`
--
ALTER TABLE `tabarticle_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_page_article_id_foreign` (`article_id`),
  ADD KEY `article_page_page_id_foreign` (`page_id`);

--
-- Индексы таблицы `tabmigrations`
--
ALTER TABLE `tabmigrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tabpages`
--
ALTER TABLE `tabpages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_alias_unique` (`alias`);

--
-- Индексы таблицы `tabpassword_resets`
--
ALTER TABLE `tabpassword_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `tabusers`
--
ALTER TABLE `tabusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tabarticles`
--
ALTER TABLE `tabarticles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `tabarticle_page`
--
ALTER TABLE `tabarticle_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `tabmigrations`
--
ALTER TABLE `tabmigrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tabpages`
--
ALTER TABLE `tabpages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `tabusers`
--
ALTER TABLE `tabusers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tabarticle_page`
--
ALTER TABLE `tabarticle_page`
  ADD CONSTRAINT `article_page_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `tabarticles` (`id`),
  ADD CONSTRAINT `article_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `tabpages` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
