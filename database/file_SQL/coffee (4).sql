-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 05 2018 г., 22:08
-- Версия сервера: 5.7.20
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `coffee`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tabarticles`
--

CREATE TABLE `tabarticles` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading_upper` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading_lower` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `range` int(11) NOT NULL DEFAULT '1',
  `design` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabarticles`
--

INSERT INTO `tabarticles` (`id`, `heading_upper`, `heading_lower`, `description`, `text`, `image`, `link_text`, `link_url`, `range`, `design`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Fresh Coffee', 'Worth Drinking', '<p class=\"mb-3\">Every cup of our quality artisan coffee starts with locally sourced, hand picked ingredients. Once you try it, our coffee will be a blissful addition to your everyday morning routine - we guarantee it! </p>', '<p class=\"mb-3\">Every cup of our quality artisan coffee starts with locally sourced, hand picked ingredients. Once you try it, our coffee will be a blissful addition to your everyday morning routine - we guarantee it! </p>', 'intro.jpg', 'Visit Us Today!', '/', 1, 'section_left_content', 'article-1', '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(2, 'Our Promise', 'To You', '<p class=\"mb-0\">When you walk into our shop to start your day, we are dedicated to providing you with friendly service, a welcoming atmosphere, and above all else, excellent products made with the highest quality ingredients. If you are not satisfied, please let us know and we will do whatever we can to make things right!</p>', '<p class=\"mb-0\">When you walk into our shop to start your day, we are dedicated to providing you with friendly service, a welcoming atmosphere, and above all else, excellent products made with the highest quality ingredients. If you are not satisfied, please let us know and we will do whatever we can to make things right!</p>', NULL, NULL, NULL, 2, 'section_information_content', 'article-2', '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(3, 'Strong Coffee, Strong Roots', 'About Our Cafe', '<p>Founded in 1987 by the Hernandez brothers, our establishment has been serving up rich coffee sourced from artisan farmers in various regions of South and Central America. We are dedicated to travelling the world, finding the best coffee, and bringing back to you here in our cafe.</p>', ' <p>Founded in 1987 by the Hernandez brothers, our establishment has been serving up rich coffee sourced from artisan farmers in various regions of South and Central America. We are dedicated to travelling the world, finding the best coffee, and bringing back to you here in our cafe.</p>\r\n  <p class=\"mb-0\">We guarantee that you will fall in <em>lust</em> with our decadent blends the moment you walk inside until you finish your last sip. Join us for your daily routine, an outing with friends, or simply just to enjoy some alone time.</p>', 'about.jpg', NULL, NULL, 2, 'section_bottom_content', 'article-abuot-1', '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(4, 'Blended to Perfection', 'Coffees &amp; Teas', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', 'products-01.jpg', NULL, NULL, 1, 'section_lists_content', 'article-product-1', '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(5, 'Delicious Treats, Good Eats', 'Bakery &amp; Kitchen', '<p class=\"mb-0\">Our seasonal menu features delicious snacks, baked goods, and even full meals perfect for breakfast or lunchtime. We source our ingredients from local, oragnic farms whenever possible, alongside premium vendors for specialty goods.</p>', '<p class=\"mb-0\">Our seasonal menu features delicious snacks, baked goods, and even full meals perfect for breakfast or lunchtime. We source our ingredients from local, oragnic farms whenever possible, alongside premium vendors for specialty goods.</p>', 'products-02.jpg', NULL, NULL, 1, 'section_lists_content', 'products-02', '2018-02-28 22:00:00', '2018-02-28 22:00:00'),
(6, 'From Around the World', 'Bulk Speciality Blends', '<p class=\"mb-0\">Travelling the world for the very best quality coffee is something take pride in. When you visit us, you\'ll always find new blends from around the world, mainly from regions in Central and South America. We sell our blends in smaller to large bulk quantities. Please visit us in person for more details.</p>', '<p class=\"mb-0\">Travelling the world for the very best quality coffee is something take pride in. When you visit us, you\'ll always find new blends from around the world, mainly from regions in Central and South America. We sell our blends in smaller to large bulk quantities. Please visit us in person for more details.</p>', 'products-03.jpg', NULL, NULL, 1, 'section_lists_content', 'products-03', '2018-02-28 22:00:00', '2018-02-28 22:00:00'),
(7, 'Come On In', 'We\'re Open', '', '', NULL, NULL, NULL, 1, 'section_information_store', 'store-1', '2018-03-02 22:00:00', '2018-03-02 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabarticle_page`
--

CREATE TABLE `tabarticle_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabarticle_page`
--

INSERT INTO `tabarticle_page` (`id`, `article_id`, `page_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 2, NULL, NULL),
(4, 4, 3, NULL, NULL),
(5, 5, 3, NULL, NULL),
(6, 6, 3, NULL, NULL),
(9, 3, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tabdesigns`
--

CREATE TABLE `tabdesigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabdesigns`
--

INSERT INTO `tabdesigns` (`id`, `name`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Section Left Content', 'section_left_content', '2018-03-02 22:00:00', '2018-03-02 22:00:00'),
(2, 'Section Bottom Content', 'section_bottom_content', '2018-03-02 22:00:00', '2018-03-02 22:00:00'),
(3, 'Section Information Content', 'section_information_content', '2018-03-02 22:00:00', '2018-03-02 22:00:00'),
(4, 'Section Information Store', 'section_information_store', '2018-03-02 22:00:00', '2018-03-02 22:00:00'),
(5, 'Section Lists Content', 'section_lists_content', '2018-03-02 22:00:00', '2018-03-02 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabmenus`
--

CREATE TABLE `tabmenus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabmenus`
--

INSERT INTO `tabmenus` (`id`, `name`, `route_name`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', 0, '2018-03-03 22:00:00', '2018-03-03 22:00:00'),
(2, 'About', 'about', 0, '2018-03-03 22:00:00', '2018-03-03 22:00:00'),
(3, 'Products', 'products', 0, '2018-03-03 22:00:00', '2018-03-03 22:00:00'),
(4, 'Store', 'store', 0, '2018-03-03 22:00:00', '2018-03-03 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabmigrations`
--

CREATE TABLE `tabmigrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabmigrations`
--

INSERT INTO `tabmigrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_28_125905_create__pages__table', 1),
(4, '2018_02_28_130954_add__parent__pages__table', 2),
(5, '2018_02_28_132855_create__articles__table', 3),
(7, '2018_03_01_170905_add__design__table__articles', 5),
(9, '2018_03_02_162451_add__range__table__articles', 7),
(10, '2018_03_02_184805_create__table__products', 8),
(11, '2018_03_02_191856_add__range__design__table__products', 9),
(15, '2018_02_28_143341_create__article__page__table', 10),
(16, '2018_03_03_203138_create__designs__table', 11),
(17, '2018_03_04_183026_crate__menus__table', 12),
(20, '2018_03_04_193627_crate__store__table', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `tabpages`
--

CREATE TABLE `tabpages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabpages`
--

INSERT INTO `tabpages` (`id`, `name`, `alias`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(2, 'About', 'about', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(3, 'Products', 'products', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00'),
(4, 'Store', 'store', 0, '2018-02-27 22:00:00', '2018-02-27 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabpassword_resets`
--

CREATE TABLE `tabpassword_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tabproducts`
--

CREATE TABLE `tabproducts` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading_upper` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading_lower` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `range` int(11) NOT NULL DEFAULT '1',
  `design` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabproducts`
--

INSERT INTO `tabproducts` (`id`, `heading_upper`, `heading_lower`, `description`, `text`, `image`, `link_text`, `link_url`, `range`, `design`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Blended to Perfection', 'Coffees &amp; Teas', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>\r\n<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', 'products-01.jpg', NULL, NULL, 1, 'section_lists_content', 'products-01', '2018-03-01 22:00:00', '2018-03-01 22:00:00'),
(2, 'Delicious Treats, Good Eats', 'Bakery &amp; Kitchen', '<p class=\"mb-0\">Our seasonal menu features delicious snacks, baked goods, and even full meals perfect for breakfast or lunchtime. We source our ingredients from local, oragnic farms whenever possible, alongside premium vendors for specialty goods.</p>', '<p class=\"mb-0\">Our seasonal menu features delicious snacks, baked goods, and even full meals perfect for breakfast or lunchtime. We source our ingredients from local, oragnic farms whenever possible, alongside premium vendors for specialty goods.</p>\r\n<p class=\"mb-0\">Our seasonal menu features delicious snacks, baked goods, and even full meals perfect for breakfast or lunchtime. We source our ingredients from local, oragnic farms whenever possible, alongside premium vendors for specialty goods.</p>', 'products-02.jpg', NULL, NULL, 1, 'section_lists_content', 'products-02', '2018-03-01 22:00:00', '2018-03-01 22:00:00'),
(3, 'From Around the World', 'Bulk Speciality Blends', '<p class=\"mb-0\">Travelling the world for the very best quality coffee is something take pride in. When you visit us, you\'ll always find new blends from around the world, mainly from regions in Central and South America. We sell our blends in smaller to large bulk quantities. Please visit us in person for more details.</p>', '<p class=\"mb-0\">Travelling the world for the very best quality coffee is something take pride in. When you visit us, you\'ll always find new blends from around the world, mainly from regions in Central and South America. We sell our blends in smaller to large bulk quantities. Please visit us in person for more details.</p>\r\n<p class=\"mb-0\">Travelling the world for the very best quality coffee is something take pride in. When you visit us, you\'ll always find new blends from around the world, mainly from regions in Central and South America. We sell our blends in smaller to large bulk quantities. Please visit us in person for more details.</p>', 'products-03.jpg', NULL, NULL, 1, 'section_lists_content', 'products-03', '2018-03-01 22:00:00', '2018-03-01 22:00:00'),
(4, 'Delicious Treats, Good Eats', 'Bakery &amp; Kitchen', '<p class=\"mb-0\">Our seasonal menu features delicious snacks, baked goods, and even full meals perfect for breakfast or lunchtime. We source our ingredients from local, oragnic farms whenever possible, alongside premium vendors for specialty goods.</p>', '<p class=\"mb-0\">Our seasonal menu features delicious snacks, baked goods, and even full meals perfect for breakfast or lunchtime. We source our ingredients from local, oragnic farms whenever possible, alongside premium vendors for specialty goods.</p>', 'products-02.jpg', NULL, NULL, 1, 'section_lists_content', 'products-04', '2018-03-04 22:00:00', '2018-03-04 22:00:00'),
(5, 'From Around the World', 'Bulk Speciality Blends', '<p class=\"mb-0\">Travelling the world for the very best quality coffee is something take pride in. When you visit us, you\'ll always find new blends from around the world, mainly from regions in Central and South America. We sell our blends in smaller to large bulk quantities. Please visit us in person for more details.</p>', '<p class=\"mb-0\">Travelling the world for the very best quality coffee is something take pride in. When you visit us, you\'ll always find new blends from around the world, mainly from regions in Central and South America. We sell our blends in smaller to large bulk quantities. Please visit us in person for more details.</p>', 'products-03.jpg', NULL, NULL, 1, 'section_lists_content', 'products-05', '2018-03-04 22:00:00', '2018-03-04 22:00:00'),
(6, 'Blended to Perfection', 'Coffees &amp; Teas', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', '<p class=\"mb-0\">We take pride in our work, and it shows. Every time you order a beverage from us, we guarantee that it will be an experience worth having. Whether it\'s our world famous Venezuelan Cappuccino, a refreshing iced herbal tea, or something as simple as a cup of speciality sourced black coffee, you will be coming back for more.</p>', 'products-01.jpg', NULL, NULL, 1, 'section_lists_content', 'products-06', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tabstore`
--

CREATE TABLE `tabstore` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_options` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_options` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tabstore`
--

INSERT INTO `tabstore` (`id`, `name_options`, `value_options`, `created_at`, `updated_at`) VALUES
(1, 'address', '{\"city\": \"Golden Valley\", \"region\": \"Minnesota\", \"phoneNumbers\": [\"(317) 585-8468\", \"(916) 123-4567\"], \"streetAddress\": \"1116 Orchard Street\"}', '2018-03-04 22:00:00', '2018-03-04 22:00:00'),
(2, 'store_time', '{\"open_store\": [\"false\", \"07:00\", \"07:00\", \"07:00\", \"07:00\", \"07:00\", \"09:00\"], \"close_store\": [\"false\", \"18:00\", \"18:00\", \"18:00\", \"18:00\", \"18:00\", \"17:00\"]}', '2018-03-04 22:00:00', '2018-03-04 22:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tabusers`
--

CREATE TABLE `tabusers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tabarticles`
--
ALTER TABLE `tabarticles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_alias_unique` (`alias`);

--
-- Индексы таблицы `tabarticle_page`
--
ALTER TABLE `tabarticle_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_page_article_id_foreign` (`article_id`),
  ADD KEY `article_page_page_id_foreign` (`page_id`);

--
-- Индексы таблицы `tabdesigns`
--
ALTER TABLE `tabdesigns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `designs_alias_unique` (`alias`);

--
-- Индексы таблицы `tabmenus`
--
ALTER TABLE `tabmenus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tabmigrations`
--
ALTER TABLE `tabmigrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tabpages`
--
ALTER TABLE `tabpages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_alias_unique` (`alias`);

--
-- Индексы таблицы `tabpassword_resets`
--
ALTER TABLE `tabpassword_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `tabproducts`
--
ALTER TABLE `tabproducts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_alias_unique` (`alias`);

--
-- Индексы таблицы `tabstore`
--
ALTER TABLE `tabstore`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `store_name_options_unique` (`name_options`);

--
-- Индексы таблицы `tabusers`
--
ALTER TABLE `tabusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tabarticles`
--
ALTER TABLE `tabarticles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `tabarticle_page`
--
ALTER TABLE `tabarticle_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `tabdesigns`
--
ALTER TABLE `tabdesigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `tabmenus`
--
ALTER TABLE `tabmenus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `tabmigrations`
--
ALTER TABLE `tabmigrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `tabpages`
--
ALTER TABLE `tabpages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `tabproducts`
--
ALTER TABLE `tabproducts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tabstore`
--
ALTER TABLE `tabstore`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `tabusers`
--
ALTER TABLE `tabusers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tabarticle_page`
--
ALTER TABLE `tabarticle_page`
  ADD CONSTRAINT `article_page_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `tabarticles` (`id`),
  ADD CONSTRAINT `article_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `tabpages` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
