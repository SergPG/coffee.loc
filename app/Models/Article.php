<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{
    //
	 
	
	
	public function pages()
    {
        return $this->belongsToMany('App\Models\Page','article_page','page_id','article_id');
    }
}
