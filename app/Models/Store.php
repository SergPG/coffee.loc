<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'store';
	
	protected $casts = [
    'value_options' => 'array',
  ];
	//
}
