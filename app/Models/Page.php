<?php

 /*
*
*
*
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /*
	*
	*/
	public function articles()
    {
        return $this->belongsToMany('App\Models\Article','article_page','page_id','article_id');
    }
	
	public function getCurrentPage( $name ){
	  return $this->where('alias',$name )->first(); 		
	}
	
	public function getArticlesPage( $name ){
	  return $this->getCurrentPage( $name )->articles->sortBy('range'); 		
	}
	
}
