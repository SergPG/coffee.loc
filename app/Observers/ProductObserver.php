<?php
namespace App\Observers;

use App\Models\Product;
use Config;

class ProductObserver
{
  /**
   * Прослушивание события создания Product.
   *
   * @param  Product  $product
   * @return void
   */
  public function saving(Product $product)
  {
    //
	
	
  }

  /**
   * Прослушивание события создания Product.
   *
   * @param  Product  $product
   * @return void
   */
  public function creating(Product $product)
  {
    //
	$tmp = $product->max('id');
	
	$product->alias = 'Product_'.($tmp+1);
	$product->design = config('coffee.design_product');
	//dd($product->alias);
	
  }

  
  /**
   * Прослушивание события удаления Product.
   *
   * @param  Product  $product
   * @return void
   */
  public function deleting(Product $product)
  {
    //
  }
}







