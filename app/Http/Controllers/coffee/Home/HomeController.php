<?php

namespace App\Http\Controllers\coffee\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\SiteController;

use Route;


class HomeController extends SiteController
{
	public function __construct(){
		
		parent::__construct();
		
		$this->metaTitle = $this->metaTitle.' - Home';
		$this->vars = array_add($this->vars,'metaTitle',$this->metaTitle);
		
		$this->routeName = Route::current()->getName();
	}
	
	public function index(){
		
		//$url = route($this->routeName);
		//dd($url);
	
		$articles = $this->modelPage->getArticlesPage( $this->routeName );
		
		$sectionsContentPage = $this->allContentPage($articles);
		$this->vars = array_add($this->vars,'sectionsContentPage',$sectionsContentPage);
		
		return view(env('THEME').'.home.index')->with($this->vars);
	}
	
}
