<?php

namespace App\Http\Controllers\coffee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\SiteMenu;
use App\Models\Page;

use Config;
use Menu;

class SiteController extends Controller
{
    //
	protected $navigationTopSite;
	protected $metaTitle;
	protected $pages;
	protected $vars = array();
	protected $routeName;
	
	protected $modelMenu;
	
	public function __construct(){
		
		$this->modelPage = new Page;
		
		$this->modelMenu = new SiteMenu;
		
		$this->metaTitle = config('coffee.site_name');
		
		$this->menus = $this->modelMenu->all();
		
		$this->navigationTopSite = Menu::get($this->getMemu('NavTop'));
		
		$this->vars = array_add($this->vars,'navigationTopSite',$this->navigationTopSite);
		
	}
	
	public function getMemu(string $nameMenu = ''){
		
		if( !empty($nameMenu )){
			\Menu::make($nameMenu, function ($menu) {
				foreach($this->menus as $menu_item){
					$item = $menu->add($menu_item->name, 
						['route'  => $menu_item->route_name,
						 'class'=>'nav-item px-lg-4']);
					$item->link->attr(['class'=>'nav-link text-uppercase text-expanded']);
				}
			});	
		}
		
			return $nameMenu;
	}
	
	public function allContentPage($articles){
		
		$sectionsContentPage = collect();
		  foreach($articles as $key => $article){
			  $item = view(env('THEME').'.layouts.design.'.$article->design,['key' =>$key, 'article'=>$article])->render();
			  $sectionsContentPage->push($item);
		  }
		return  $sectionsContentPage;
		
	}
	
	
}
