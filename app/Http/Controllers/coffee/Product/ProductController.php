<?php

namespace App\Http\Controllers\coffee\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\SiteController;

use App\Models\Product;

use Route;



class ProductController extends SiteController
{
    public function __construct(){
		
		parent::__construct();
		
		$this->metaTitle = $this->metaTitle.' - Products';
		$this->vars = array_add($this->vars,'metaTitle',$this->metaTitle);
		
		$this->routeName = Route::current()->getName();
	}
	
	public function index(){
		
		$products = Product::paginate(3);
		
		$this->vars = array_add($this->vars,'products',$products);
		
		//dd($products);
		
		$articles = $this->modelPage->getArticlesPage( $this->routeName );
		
		$sectionsContentPage = $this->allContentPage($articles);
		$this->vars = array_add($this->vars,'sectionsContentPage',$sectionsContentPage);
		
		return view(env('THEME').'.products.index')->with($this->vars);
	}
	
	
}
