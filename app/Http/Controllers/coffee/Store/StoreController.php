<?php

namespace App\Http\Controllers\coffee\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\SiteController;

use App\Models\Store;
use Route;

class StoreController extends SiteController
{
    public function __construct(){
		
		parent::__construct();
		
		$this->metaTitle = $this->metaTitle.' - Store';
		$this->vars = array_add($this->vars,'metaTitle',$this->metaTitle);
		
		$this->routeName = Route::current()->getName();
	}
	
	public function index(){
        
        $store = Store::where('name_options', 'address')->first();
		
		$address = $store->value_options;
		
		
		$store = Store::where('name_options', 'store_time')->first();
		$store_time = $store->value_options;
		
		//dd($address, $store_time);
		
	    $this->vars = array_add($this->vars,'address',$address);
	    $this->vars = array_add($this->vars,'store_time',$store_time);
			
		$articles = $this->modelPage->getArticlesPage( $this->routeName );
		
		$sectionsContentPage = $this->allContentPage($articles);
		$this->vars = array_add($this->vars,'sectionsContentPage',$sectionsContentPage);
		
		return view(env('THEME').'.store.index')->with($this->vars);
	}
	
}
