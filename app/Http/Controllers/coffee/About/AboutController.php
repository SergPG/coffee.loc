<?php

namespace App\Http\Controllers\coffee\About;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\SiteController;

use Route;

class AboutController extends SiteController
{
    public function __construct(){
		
		parent::__construct();
		
		$this->metaTitle = $this->metaTitle.' - About';
		$this->vars = array_add($this->vars,'metaTitle',$this->metaTitle);
		
		//====================  Дальше одинаково =============================
		$this->routeName = Route::current()->getName();
	}
	
	public function index(){
		
		$articles = $this->modelPage->getArticlesPage( $this->routeName );
		
		$sectionsContentPage = $this->allContentPage($articles);
		$this->vars = array_add($this->vars,'sectionsContentPage',$sectionsContentPage);
			
		return view(env('THEME').'.about.index')->with($this->vars);
	}
	
	
}
