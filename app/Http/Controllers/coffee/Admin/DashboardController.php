<?php

namespace App\Http\Controllers\coffee\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\Admin\AdminController;

use Menu;

class DashboardController extends AdminController
{
    //
	
	public function __construct(){
		parent :: __construct();
		
	}
	
	public function index(){

		
		return view(env('THEME').'.admin.index')->with($this->vars);
	}
}
