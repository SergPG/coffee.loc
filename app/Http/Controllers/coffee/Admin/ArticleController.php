<?php

namespace App\Http\Controllers\coffee\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\Admin\AdminController;

use Menu;
use Carbon\Carbon;

use App\Models\Article;

class ArticleController extends AdminController
{ 

    protected $model;
	
    protected $nameModel;
    

    public function __construct( Article $model){
		
		parent::__construct();
		
		$this->model = $model;
		
		//$this->title = get_class($this->model);
		
		$this->title = 'Articles';
		$this->vars = array_add($this->vars,'title',$this->title);
		
	}  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$articles = $this->model->all();
		$this->vars = array_add($this->vars,'articles',$articles); 
		
		
		//$tmp = $this->model->find(2)->created_at->format(' d M Y');
		//dd($tmp);
		
		
		return view(env('THEME').'.admin.Article.index')->with($this->vars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
