<?php

namespace App\Http\Controllers\coffee\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\Admin\AdminController;

use App\Models\Product;
use Carbon\Carbon;

use Config;

use App\Http\Requests\UploadImage;

use App\Http\Requests\NewProduct;

class ProductController extends AdminController
{
   
	/*
	* Product Model
	*
	*   string('heading_upper');
	*	string('heading_lower');
	*	text('description');
	*	text('text');
	*	string('image')->nullable();;
	*	string('link_text')->nullable();
	*	string('link_url')->nullable();
	*	integer('range')->default(1);
	*	string('design')->nullable(); section_lists_content
	*	string('alias')->unique();
	*
	*   timestamps();
	*/
   
   
   
   
	protected $model;
	
	protected $image_name ;
	
	
	
	
	
    protected $nameModel;
    

    public function __construct( Product $model){
		
		parent::__construct();
		
		$this->model = $model;
		
		//$this->title = get_class($this->model);
		
		$this->title = 'Products';
		$this->vars = array_add($this->vars,'title',$this->title);

    }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
	
	
    {
        //
		$products = $this->model->paginate(3);
		$this->vars = array_add($this->vars,'products',$products);
		
	   // $tmp = $this->model->find(2)->created_at->format(' d M Y');
		//dd($tmp);
		
		
		//dd($products);
		
		return view(env('THEME').'.admin.Product.index')->with($this->vars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$tmp = config('coffee.path_img_product');
		dd($tmp);
		
		return view(env('THEME').'.admin.Product.create')->with($this->vars );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewProduct $request)
    {
       
	//	$tmp = $this->model->id;
		
	//	dd($tmp);

        $this->model->heading_upper = $request->heading_upper;
		$this->model->heading_lower = $request->heading_lower;
		
		$this->model->description = $request->description;
		$this->model->text = $request->description;
		
		$this->model->image = $request->image;
		
		$this->model->save();
		
		//dd($heading_upper);
		
		
	//	$method = $request->method();
		
	//	$input = $request->all();
		
		//dd($method, $input );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		 $product = $this->model->find($id);
		 $this->vars = array_add($this->vars,'product',$product);
		 return view(env('THEME').'.admin.Product.edit')->with($this->vars);
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
	
		$product = $this->model->find($id);
		$this->vars = array_add($this->vars,'product',$product);
		
	    $this->image_name = $product->image;
		$this->vars = array_add($this->vars,'image_name',$this->image_name);
		
		
		//dd($products);
		
		return view(env('THEME').'.admin.Product.edit')->with($this->vars);
		
		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$uri = $request->path();
		
		if ($request->is('admin/*')) {
		 $url = $request->url();
		 
		 $urlFl = $request->fullUrl();
		 
		 $url2 =  $request->fullUrlWithQuery(['bar' => 'baz']);
		}
		
		$method = $request->method();
		
		$input = $request->all();
		
		$inputName = $request->input('heading_upper');
		
		dd($uri, $url, $urlFl, $url2, $method , $input, $inputName);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	
	
	/* public function storeUploadImage(UploadImage $request)
   {
         	 
	  $uri = $request->path();
		
		
		
		if($request->hasFile('img')){
			$file = $request->file('img');
			
		   return back()->withInput()->with('status', 'Profile updated!');
		
		  // return redirect()->action('coffee\Admin\ProductController@index');
			
			//return redirect()->view(env('THEME').'.admin.Product.edit');
		}
			return back()->withInput()->with('status', 'Profile NOT updated!');

			
		//	return view(env('THEME').'.layouts.design.admin.admin_Upload_Image_Modal');
		}
		
		else{ 
		
		$input = $request->all();
		
	
		
		//dd($uri,  $input);
		
		
	   
   }	
	*/
	
	
	
}
