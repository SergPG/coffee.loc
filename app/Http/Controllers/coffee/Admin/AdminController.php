<?php

namespace App\Http\Controllers\coffee\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Config;
use Menu;

class AdminController extends Controller
{
    //
	protected $vars = array();
	
	protected $breadcrumbs = array();
	
	protected $mAdminNavbar ;
	
	
	
	
	//protected string $link_name = 'Dashboard';
	//protected string $route_name = '';
	
	
	
	
	public function __construct(){
		
		$this->mAdminNavbar = $this->getAdminNavbar(); 
		
		$this->vars = array_add($this->vars,'mAdminNavbar',$this->mAdminNavbar); 
		
		//dd($this->vars);
		
	}
	
	public function getAdminNavbar(){
		$result = Menu::make('AdminNavbar', function ($m) {
		   		
			$m->add('Dashboard',['route'  => 'admin'])->nickname('fa-dashboard');
		//	$m->add('Charts',['route'  => 'components.cards'])->nickname('fa-area-chart');
			
			$m->add('Add a new one',null)->nickname('fa-file');
			  $m->item('fa-file')->add('New Article',['url' => 'admin/pages'])->nickname('articleNew');
			  $m->item('fa-file')->add('New Product',['url' => 'admin/products/create'])->nickname('productNew');	
			  
			  $m->add('Tables',null)->nickname('fa-table');
			   $m->item('fa-table')->add('Articles Table',['url' => 'admin/articles'])->nickname('articlesTable');
			   $m->item('fa-table')->add('Products Tables',['url' => 'admin/products'])->nickname('productsTable');
			   $m->item('fa-table')->add('Users Tables',['url' => 'admin/users'])->nickname('usersTable');
			
			$m->add('Components',null)->nickname('fa-wrench');
			  $m->item('fa-wrench')->add('Navbar',['route'  => 'components.cards'])->nickname('navbar');
			  $m->item('fa-wrench')->add('Cards',['route'  => 'components.cards'])->nickname('cards');
			
			
			$m->add('Link',['route'  => 'components.cards'])->nickname('fa-link');
			});
		
		return $result;
	}
	
	
	
	
}
