<?php

namespace App\Http\Controllers\coffee\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\coffee\Admin\AdminController;

use Menu;
use Route; 

class CardController extends AdminController
{
    //
	public function __construct(){
		parent :: __construct();
		
		
		$my_route = Route::getCurrentRoute()->getName();;
		//dd($my_route);
		
	}
	
	public function index(){
		
		$brd =  Menu::get('Breadcrumb');
		$brd->add('Dashboard',['route'  => 'admin','class'=>'breadcrumb-item']);
		$brd->add('Cards',['route'  => null,'class'=>'breadcrumb-item'])->active();
		//dd($brd);
		
		return view(env('THEME').'.admin.index')->with($this->vars);
	}
}
